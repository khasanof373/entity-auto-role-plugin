# Entity Auto Role Plugin

this entity auto role plugin is released to reduce roles. You should mainly use the plugin jhipster for promoted
projects.
Usage example.

```java

@RestController
@RequestMapping("/api")
public class CountryResource {

    @PostMapping("/countries") // COUNTRY_R
    public ResponseEntity<CountryDTO> createCountry(@RequestBody CountryDTO countryDTO) throws URISyntaxException {
        // ...
    }

    @PutMapping("/countries/{id}") // COUNTRY_U
    public ResponseEntity<CountryDTO> updateCountry(
            @PathVariable(value = "id", required = false) final Long id,
            @RequestBody CountryDTO countryDTO
    ) throws URISyntaxException {
        // ...
    }

    // COUNTRY_U
    @PatchMapping(value = "/countries/{id}", consumes = {"application/json", "application/merge-patch+json"})
    public ResponseEntity<CountryDTO> partialUpdateCountry(
            @PathVariable(value = "id", required = false) final Long id,
            @RequestBody CountryDTO countryDTO
    ) throws URISyntaxException {
        // ...
    }

    
    @GetMapping("/countries") // COUNTRY_R
    public List<CountryDTO> getAllCountries() {
        // ...
    }

    @GetMapping("/countries/v2") // COUNTRY_V2
    public List<CountryDTO> allCountriesV2() {
        // ...
    }

    @PostMapping({"/countries/v4/{id}"}) // no role
    @GetMapping({"/countries/v2/{id}", "/countries/v3/{id}"})
    public List<CountryDTO> allCountriesV2(@PathVariable String id) {
        // ...
    }

}
```

```java

@Component
public class SimpleAuthorizeConfig implements AuthorizeConfig {

    @Override
    public Set<EntityAuthorize> getAuthorities() {
        return Set.of(
                new EntityAuthorize(Country.class, true)
        );
    }

    @Override
    public String path() {
        return "com.mycompany";
    }

}
```

the simplest configuration to use. this configuration finds the controller for the Country object and checks its methods
and assigns an auto role to methods that start with name and match httpMethod .

- method name start with - `create` and if annotation placed - `@PostMapping` : gives the entity name_C role
- method name start with - `update` and if annotation placed - `@PutMapping` : gives the entity name_U role
- method name start with - `partialUpdate` and if annotation placed - `@PatchMapping` : gives the entity name_C role
- method name start with - `getAll` and if annotation placed - `@GetMapping` : gives the entity name_R role
- method name start with - `count` and if annotation placed - `@PatchMapping` : gives the entity name_R role
- method name start with - `get` and if annotation placed - `@GetMapping` : gives the entity name_R role
- method name start with - `delete` and if annotation placed - `@DeleteMapping` : gives the entity name_D role

If a method matching the above is found, it will be assigned an auto role if you set
EntityAuthorize.setDefaultAuthorizes to true. if you do not give the name of the entity, it will automatically take the
entity simple name and mark it as upper case for the api.

In addition, you can customize the APIs you want. There are 2 ways to customize

1. you can close by url
2. You can close by method name

## Close by url

```java
@Override
public Set<EntityAuthorize> getAuthorities() {
    return Set.of(
        new EntityAuthorize(Country.class, true)
                .customize(new UrlAuthorize("/countries/v2", RequestMethod.GET, "V2"))
    );
}
```

## Close by method name

```java
@Override
public Set<EntityAuthorize> getAuthorities() {
        return Set.of(
            new EntityAuthorize(Country.class, true)
                    .customize(new MethodAuthorize("allCountriesV2", "V2"))
        )
    );
}
```

## JWT token claims


![img](static/images/image_2023-08-16_11-02-56.png)


```yaml
entity:
  auto-role:
    claim-path: realm_access.roles
    implement-count: 2
    primary-config-name: simpleAuthorizeConfig
```

## Open Path Config

We can specify which API should be open by overriding AuthorizeConfig interface openPaths() method.

```java
@Override
public Set<Url> openPaths() {
    return Set.of(new Url("/api/categories/**"),
        new Url("/api/keycloak/get-all-users", RequestMethod.GET));
}
```
