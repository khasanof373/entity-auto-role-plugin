package uz.devops.khasanof.entityautoroleplugin;

import uz.devops.khasanof.entityautoroleplugin.domain.EntityAuthorize;
import uz.devops.khasanof.entityautoroleplugin.domain.Url;

import java.util.HashSet;
import java.util.Set;

/**
 * The AuthorizeConfig interface is used to create auto permissions for APIs.
 * it should be a single implementation, no more, no less.
 *
 *  <h2>Example Usage</h2>
 *
 *  <pre>
 *  &#064;Component
 *  public class SimpleAuthorizeConfig implements AuthorizeConfig {
 *
 *     &#064;Override
 *     public Set<EntityAuthorize> getAuthorities() {
 *         return Set.of(
 *             new EntityAuthorize(Country.class, true),
 *             new EntityAuthorize(Department.class, true),
 *             new EntityAuthorize(Employee.class, true),
 *             new EntityAuthorize(CountryService.class, true)
 *         );
 *     }
 *
 * }
 *  </pre>
 *
 * The implementation of the AuthorizeConfig interface must be a Bean and all
 * methods must be written completely as shown.
 *
 * @author Nurislom, Abdulloh
 * @see uz.devops.khasanof.entityautorolepluigin
 * @since 31.07.2023 16:55
 */
public interface AuthorizeConfig {

    /**
     * The AuthorizeConfig#getAuthorities method must not return null!.
     * because if it is null it will block any incoming requests because no entity was found.
     * we need to declare the entities in the program so that it finds the APIs related to this entity
     * and gives automatic permission.
     *
     * <h2>Simple Example Usage</h2>
     *
     * <pre>
     *     &#064;Override
     *     public Set<EntityAuthorize> getAuthorities() {
     *         return Set.of(
     *             new EntityAuthorize(Country.class, true),
     *             new EntityAuthorize(Department.class, true),
     *             new EntityAuthorize(Employee.class, true),
     *             new EntityAuthorize(CountryService.class, true)
     *         );
     *     }
     * </pre>
     *
     * @return should return a list of entities. cannot be null!
     */
    Set<EntityAuthorize> getAuthorities();

    /**
     *
     * @return open APIs
     */
    default Set<Url> openPaths() {
        return new HashSet<>();
    }

}
