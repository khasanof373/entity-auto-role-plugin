package uz.devops.khasanof.entityautoroleplugin;

/**
 * @author Nurislom
 * @see uz.devops.khasanof.entityautoroleplugin
 * @since 11/3/2023 11:39 PM
 */
public interface AuthorizeConfigurer<T> {

    T and();

}
