package uz.devops.khasanof.entityautoroleplugin;

/**
 * @author Nurislom
 * @see uz.devops.khasanof.entityautoroleplugin
 * @since 11/7/2023 11:20 PM
 */
public interface AuthorizeConfigurerAdapter {

    CustomAuthorizeConfigurer<AuthorizeConfigurerAdapter> customAuthorizeConfigurer();

    EntityAuthorizeConfigurer<AuthorizeConfigurerAdapter> entityAuthorizeConfigurer();

    GroupAuthorizeConfigurer<AuthorizeConfigurerAdapter> groupAuthorizeConfigurer();

}
