package uz.devops.khasanof.entityautoroleplugin;

import org.springframework.boot.autoconfigure.AutoConfigurationExcludeFilter;
import org.springframework.boot.context.TypeExcludeFilter;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import uz.devops.khasanof.entityautoroleplugin.core.config.PluginProperties;

/**
 * @author Nurislom
 * @see uz.devops.khasanof.entityautoroleplugin
 * @since 8/5/2023 2:00 PM
 */
@ComponentScan(
        excludeFilters = {@ComponentScan.Filter(
                type = FilterType.CUSTOM,
                classes = {TypeExcludeFilter.class}
        ), @ComponentScan.Filter(
                type = FilterType.CUSTOM,
                classes = {AutoConfigurationExcludeFilter.class}
        )}
)
@EnableConfigurationProperties(value = {PluginProperties.class})
public class AutoConfiguration {
}
