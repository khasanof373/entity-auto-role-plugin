package uz.devops.khasanof.entityautoroleplugin;

import uz.devops.khasanof.entityautoroleplugin.domain.CustomMethodAuthorize;
import uz.devops.khasanof.entityautoroleplugin.domain.CustomMethodsAuthorize;
import uz.devops.khasanof.entityautoroleplugin.domain.CustomUriAuthorize;
import uz.devops.khasanof.entityautoroleplugin.domain.CustomUrisAuthorize;

/**
 * @author Nurislom
 * @see uz.devops.khasanof.entityautoroleplugin
 * @since 11/3/2023 11:46 PM
 */
public interface CustomAuthorizeConfigurer<T> extends AuthorizeConfigurer<T> {

    CustomAuthorizeConfigurer<T> customMethods(CustomMethodAuthorize... authorizes);

    CustomAuthorizeConfigurer<T> customUris(CustomUriAuthorize... authorizes);

    CustomAuthorizeConfigurer<T> customMethods(CustomMethodsAuthorize... authorizes);

    CustomAuthorizeConfigurer<T> customUris(CustomUrisAuthorize... authorizes);

}
