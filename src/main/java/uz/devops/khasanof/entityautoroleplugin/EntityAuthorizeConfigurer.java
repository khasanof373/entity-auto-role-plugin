package uz.devops.khasanof.entityautoroleplugin;

import uz.devops.khasanof.entityautoroleplugin.domain.EntityAuthorize;

import java.util.Set;

/**
 * @author Nurislom
 * @see uz.devops.khasanof.entityautoroleplugin
 * @since 11/3/2023 11:41 PM
 */
public interface EntityAuthorizeConfigurer<T> extends AuthorizeConfigurer<T> {

    EntityAuthorizeConfigurer<T> entityAuthorizes(Set<EntityAuthorize> authorizes);

    EntityAuthorizeConfigurer<T> entityAuthorizes(EntityAuthorize... authorizes);

}
