package uz.devops.khasanof.entityautoroleplugin;

import uz.devops.khasanof.entityautoroleplugin.domain.AllOfAuthorize;
import uz.devops.khasanof.entityautoroleplugin.domain.CrudOfAuthorize;

/**
 * @author Nurislom
 * @see uz.devops.khasanof.entityautoroleplugin
 * @since 11/7/2023 11:13 PM
 */
public interface GroupAuthorizeConfigurer<T> extends AuthorizeConfigurer<T> {

    GroupAuthorizeConfigurer<T> allOf(AllOfAuthorize... authorizes);

    GroupAuthorizeConfigurer<T> crudOf(CrudOfAuthorize... authorizes);

}
