package uz.devops.khasanof.entityautoroleplugin.core;

import java.util.Set;

/**
 * @author Nurislom
 * @see uz.devops.khasanof.entityautoroleplugin.core
 * @since 01.08.2023 16:16
 */
public interface EntityCollector<T> {

    Set<T> getAll();

    Class<T> getType();

    default boolean hasEntities() {
        return false;
    }

    default void addEntity(Set<T> ts) {
        throw new RuntimeException("Not Implemented!");
    }

}
