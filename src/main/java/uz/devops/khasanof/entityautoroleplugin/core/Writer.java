package uz.devops.khasanof.entityautoroleplugin.core;

import uz.devops.khasanof.entityautoroleplugin.domain.EntityAuthorize;

import java.util.Set;

/**
 * @author Nurislom
 * @see uz.devops.khasanof.entityautorolepluigin.core
 * @since 01.08.2023 16:21
 */
public interface Writer {

    void execute(Set<EntityAuthorize> list);

}
