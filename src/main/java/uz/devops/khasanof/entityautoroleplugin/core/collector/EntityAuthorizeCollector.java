package uz.devops.khasanof.entityautoroleplugin.core.collector;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import uz.devops.khasanof.entityautoroleplugin.AuthorizeConfig;
import uz.devops.khasanof.entityautoroleplugin.core.collector.checker.GenericChecker;
import uz.devops.khasanof.entityautoroleplugin.core.EntityCollector;
import uz.devops.khasanof.entityautoroleplugin.core.collector.checker.SimpleAuthorizeChecker;
import uz.devops.khasanof.entityautoroleplugin.core.collector.checker.SimpleUrlChecker;
import uz.devops.khasanof.entityautoroleplugin.core.config.PluginProperties;
import uz.devops.khasanof.entityautoroleplugin.domain.EntityAuthorize;

import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * @author Nurislom
 * @see uz.devops.khasanof.entityautoroleplugin.core.collector
 * @since 01.08.2023 16:18
 */
@Slf4j
@Service(EntityAuthorizeCollector.NAME)
public class EntityAuthorizeCollector implements EntityCollector<EntityAuthorize>, InitializingBean {

    public static final String NAME = "entityAuthorizeCollector";
    private final ApplicationContext applicationContext;
    private final Set<EntityAuthorize> authorizeSet = new HashSet<>();
    private final PluginProperties properties;

    public EntityAuthorizeCollector(ApplicationContext applicationContext, PluginProperties properties) {
        this.applicationContext = applicationContext;
        this.properties = properties;
    }

    @Override
    public Set<EntityAuthorize> getAll() {
        return this.authorizeSet;
    }

    @Override
    public Class<EntityAuthorize> getType() {
        return EntityAuthorize.class;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        Map<String, AuthorizeConfig> beans = applicationContext.getBeansOfType(AuthorizeConfig.class);
        GenericChecker authorizeChecker = applicationContext.getBean(SimpleAuthorizeChecker.NAME, GenericChecker.class);
        AuthorizeConfig collector = getAuthorize(beans);
        if (Objects.isNull(collector.getAuthorities())) {
            throw new RuntimeException("Authorities must not be null!");
        }
        boolean allMatch = collector.getAuthorities().stream().allMatch(authorizeChecker::valid);
        if (allMatch) {
            authorizeSet.addAll(collector.getAuthorities());
        }
        openUrlCheck(collector);
    }

    private AuthorizeConfig getAuthorize(Map<String, AuthorizeConfig> configMap) {
        Map.Entry<Boolean, Boolean> checked = checkImplementCount(configMap);
        if (checked.getKey()) {
            if (checked.getValue()) {
                return configMap.get(properties.getPrimaryConfigName());
            } else {
                return configMap.values().iterator().next();
            }
        }
        throw new RuntimeException();
    }

    private Map.Entry<Boolean, Boolean> checkImplementCount(Map<String, AuthorizeConfig> configMap) {
        if (properties.getImplementCount() < 1) {
            throw new RuntimeException("implementCount cannot be 0 or less.");
        }
        if (properties.getImplementCount() == 1) {
            if (configMap.size() != 1) {
                throw new RuntimeException("AuthorizeConfig interface must be one implement!");
            }
        } else {
            if (!(configMap.size() <= properties.getImplementCount())) {
                throw new RuntimeException("authorize config interface must not have more beans than implement count!");
            }
            if (Objects.nonNull(properties.getPrimaryConfigName())) {
                return Map.entry(true, true);
            }
        }
        return Map.entry(true, false);
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private void openUrlCheck(AuthorizeConfig collector) {
        if (Objects.isNull(collector.openPaths())) {
            log.info("open paths not found!");
        } else {
            if (collector.openPaths().isEmpty()) {
                log.warn("open paths is empty!");
            } else {
                GenericChecker checker = applicationContext.getBean(SimpleUrlChecker.NAME, GenericChecker.class);
                boolean allMatch1 = collector.openPaths().stream().allMatch(checker::valid);
                if (allMatch1) {
                    EntityCollector urlCollector = applicationContext.getBean(OpenUrlCollector.NAME, EntityCollector.class);
                    urlCollector.addEntity(collector.openPaths());
                } else {
                    log.warn("invalid url found!");
                }
            }
        }
    }
}
