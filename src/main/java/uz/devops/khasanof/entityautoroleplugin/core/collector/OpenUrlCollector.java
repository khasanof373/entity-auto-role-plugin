package uz.devops.khasanof.entityautoroleplugin.core.collector;

import org.springframework.stereotype.Service;
import uz.devops.khasanof.entityautoroleplugin.core.EntityCollector;
import uz.devops.khasanof.entityautoroleplugin.domain.Url;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Nurislom
 * @see uz.devops.khasanof.entityautoroleplugin.core.collector
 * @since 8/17/2023 12:05 PM
 */
@Service(OpenUrlCollector.NAME)
public class OpenUrlCollector implements EntityCollector<Url> {

    public static final String NAME = "openUrlCollector";
    private final Set<Url> urls = new HashSet<>();

    @Override
    public Set<Url> getAll() {
        return this.urls;
    }

    @Override
    public boolean hasEntities() {
        return !urls.isEmpty();
    }

    @Override
    public void addEntity(Set<Url> list) {
        this.urls.addAll(list);
    }

    @Override
    public Class<Url> getType() {
        return Url.class;
    }
}
