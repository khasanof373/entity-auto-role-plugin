package uz.devops.khasanof.entityautoroleplugin.core.collector;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import uz.devops.khasanof.entityautoroleplugin.core.EntityCollector;
import uz.devops.khasanof.entityautoroleplugin.domain.Resource;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * @author Nurislom
 * @see uz.devops.khasanof.entityautoroleplugin.core.collector
 * @since 8/4/2023 4:42 PM
 */
@Slf4j
@Component
public class ResourceCollector implements EntityCollector<Resource> {

    private final Set<Resource> resources = new HashSet<>();

    @Override
    public Set<Resource> getAll() {
        return this.resources;
    }

    @Override
    public void addEntity(Set<Resource> list) {
        if (Objects.nonNull(list) && !list.isEmpty()) {
            this.resources.addAll(list);
        } else {
            log.warn("resource is null or empty!");
        }
    }

    @Override
    public Class<Resource> getType() {
        return Resource.class;
    }
}
