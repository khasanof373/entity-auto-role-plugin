package uz.devops.khasanof.entityautoroleplugin.core.collector.checker;

/**
 * @author Nurislom
 * @see uz.devops.khasanof.entityautoroleplugin.core.collector
 * @since 02.08.2023 12:54
 */
public interface GenericChecker<T> {

    boolean valid(T t);

}
