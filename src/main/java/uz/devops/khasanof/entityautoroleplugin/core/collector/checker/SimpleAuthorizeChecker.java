package uz.devops.khasanof.entityautoroleplugin.core.collector.checker;

import org.springframework.stereotype.Service;
import uz.devops.khasanof.entityautoroleplugin.core.util.BaseUtils;
import uz.devops.khasanof.entityautoroleplugin.domain.EntityAuthorize;
import uz.devops.khasanof.entityautoroleplugin.domain.MethodAuthorize;
import uz.devops.khasanof.entityautoroleplugin.domain.UrlAuthorize;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;

/**
 * @author Nurislom
 * @see uz.devops.khasanof.entityautoroleplugin.core.collector.checker
 * @since 02.08.2023 12:55
 */
@Service(SimpleAuthorizeChecker.NAME)
public class SimpleAuthorizeChecker implements GenericChecker<EntityAuthorize> {

    public static final String NAME = "simpleAuthorizeChecker";
    private final List<String> invalidChars = List.of("_", "$", "%", "^", "&", "!");

    @Override
    public boolean valid(EntityAuthorize entityAuthorize) {
        if (entityAuthorize.isSetDefaultAuthorizes()) {
            BaseUtils.checkArgsIsNonNull(entityAuthorize.getDomainClassType());
            if (Objects.nonNull(entityAuthorize.getEntityPrefix())) {
                validAnyPrefix(entityAuthorize::getEntityPrefix);
            }
            return true;
        } else {
            BaseUtils.checkArgsIsNonNull(entityAuthorize.getDomainClassType(), entityAuthorize.getEntityPrefix());
            validAnyPrefix(entityAuthorize::getEntityPrefix);
            return listNonNull(entityAuthorize.getMethodAuthorizes(), entityAuthorize.getUrlAuthorizes());
        }
    }

    private boolean listNonNull(List<MethodAuthorize> methodAuthorizes, List<UrlAuthorize> urlAuthorizes) {
        if (Objects.isNull(methodAuthorizes) && Objects.isNull(urlAuthorizes)) {
            return false;
        }
        if (Objects.nonNull(methodAuthorizes) && Objects.nonNull(urlAuthorizes)) {
            return false;
        }
        if (Objects.isNull(methodAuthorizes)) {
            checkUrlAuthorizes(urlAuthorizes);
            urlAuthorizes.forEach(urlAuthorize -> validAnyPrefix(urlAuthorize::getRoleSuffix));
        } else  {
            checkMethodAuthorizes(methodAuthorizes);
            methodAuthorizes.forEach(methodAuthorize -> validAnyPrefix(methodAuthorize::getRoleSuffix));
        }
        return true;
    }

    private void checkUrlAuthorizes(List<UrlAuthorize> urlAuthorizes) {
        for (UrlAuthorize urlAuthorize : urlAuthorizes) {
            if (isBlank(urlAuthorize.getUrl())) {
                throw new RuntimeException("url must not be null or blank!");
            }
            if (isBlank(urlAuthorize.getRoleSuffix())) {
                throw new RuntimeException("url must not be null or blank!");
            }
        }
    }

    private void checkMethodAuthorizes(List<MethodAuthorize> methodAuthorizes) {
        for (MethodAuthorize methodAuthorize : methodAuthorizes) {
            if (isBlank(methodAuthorize.getMethodName())) {
                throw new RuntimeException("method name must not be null or blank!");
            }
            if (isBlank(methodAuthorize.getRoleSuffix())) {
                throw new RuntimeException("method role prefix must not be null or blank!");
            }
        }
    }

    private static boolean isBlank(String var) {
        return Objects.isNull(var) || var.isBlank();
    }

    private void validAnyPrefix(Supplier<String> supplier) {
        String s = supplier.get();
        boolean anyMatch = invalidChars.stream().anyMatch(s::startsWith);
        if (anyMatch) {
            throw new RuntimeException("You cannot add such characters to role or entity prefixes. : " + Arrays.toString(invalidChars.toArray()));
        }
    }

}
