package uz.devops.khasanof.entityautoroleplugin.core.collector.checker;

import org.springframework.stereotype.Service;
import uz.devops.khasanof.entityautoroleplugin.domain.Url;

import java.util.Objects;

/**
 * @author Nurislom
 * @see uz.devops.khasanof.entityautoroleplugin.core.collector.checker
 * @since 8/17/2023 11:59 AM
 */
@Service(SimpleUrlChecker.NAME)
public class SimpleUrlChecker implements GenericChecker<Url> {

    public static final String NAME = "simpleUrlChecker";

    @Override
    public boolean valid(Url url) {
        return Objects.nonNull(url.getUri()) && !url.getUri().isBlank();
    }

}
