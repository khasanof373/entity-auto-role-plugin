package uz.devops.khasanof.entityautoroleplugin.core.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author Nurislom
 * @see uz.devops.khasanof.entityautoroleplugin.core.config
 * @since 8/5/2023 1:55 PM
 */
@Getter
@Setter
@ConfigurationProperties(prefix = "entity.auto-role")
public class PluginProperties {

    private String claimPath;
    private int implementCount = 1;
    private String primaryConfigName;

}
