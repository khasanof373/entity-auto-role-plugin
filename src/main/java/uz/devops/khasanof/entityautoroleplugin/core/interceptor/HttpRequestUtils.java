package uz.devops.khasanof.entityautoroleplugin.core.interceptor;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * @author Nurislom
 * @see uz.devops.khasanof.entityautoroleplugin.core.interceptor
 * @since 8/5/2023 3:12 PM
 */
public abstract class HttpRequestUtils {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    public static String getToken(HttpServletRequest request) {
        String authorization = request.getHeader(HttpHeaders.AUTHORIZATION);
        if (Objects.nonNull(authorization) && authorization.startsWith("Bearer ")) {
            return authorization.substring("Bearer ".length());
        }
        return null;
    }

    public static void predicateNoneMatch(Supplier<Boolean> supplier, Runnable runnable) {
        if (!supplier.get()) runnable.run();
    }

    @SneakyThrows
    public static void responseWriter(HttpServletResponse response, Integer code, String message) {
        response.setStatus(code);
        Map<String, Object> responseMap = new HashMap<>() {{
            put("code", code);
            put("message", message);
        }};
        OBJECT_MAPPER.writeValue(response.getOutputStream(), responseMap);
    }

    @SneakyThrows
    public static void responseWriter(HttpServletResponse response) {
        response.setStatus(HttpStatus.FORBIDDEN.value());
        Map<String, Object> responseMap = new HashMap<>() {{
            put("code", HttpStatus.FORBIDDEN.value());
            put("message", "access denied!");
        }};
        OBJECT_MAPPER.writeValue(response.getOutputStream(), responseMap);
    }

}
