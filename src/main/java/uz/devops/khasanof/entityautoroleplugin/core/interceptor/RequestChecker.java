package uz.devops.khasanof.entityautoroleplugin.core.interceptor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.bind.annotation.RequestMethod;
import uz.devops.khasanof.entityautoroleplugin.core.EntityCollector;
import uz.devops.khasanof.entityautoroleplugin.core.util.BaseUtils;
import uz.devops.khasanof.entityautoroleplugin.domain.Resource;
import uz.devops.khasanof.entityautoroleplugin.domain.Url;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Nurislom
 * @see uz.devops.khasanof.entityautoroleplugin.core.interceptor
 * @since 8/5/2023 2:36 PM
 */
@Slf4j
@Component
public class RequestChecker {

    private final AntPathMatcher antPathMatcher = new AntPathMatcher();

    public boolean check(String uri, RequestMethod method, EntityCollector<Url> collector) {
        if (collector.hasEntities()) {
            Set<Url> urls = collector.getAll();
            Optional<Url> optional = urls.stream().filter(url -> antPathMatcher.match(url.getUri(), uri) &&
                    matchMethod(method, url.getHttpMethod())).findFirst();
            return optional.isPresent();
        }
        return false;
    }

    private boolean matchMethod(RequestMethod requestMethod, RequestMethod urlMethod) {
        if (Objects.isNull(urlMethod)) {
            return true;
        }
        return urlMethod.equals(requestMethod);
    }

    public boolean check(String uri, RequestMethod method, List<GrantedAuthority> authorities, EntityCollector<Resource> collector) {
        Set<Resource> resources = collector.getAll();
        if (Objects.isNull(resources) || resources.isEmpty()) {
            log.warn("resources is null or empty!");
            return false;
        }

        Resource result = resources.stream().filter(resource -> antMatcher(uri, method, resource))
                .findFirst().orElse(null);

        if (Objects.isNull(result)) return false;
        if (Objects.isNull(authorities) || authorities.isEmpty()) return false;

        return matchAuthority(result.getRole(), authorities);
    }

    private boolean antMatcher(String uri, RequestMethod method, Resource resource) {
        boolean matchUri, matchMethod = true;
        matchUri = antPathMatcher.match(resource.getUrl(), uri);
        if (Objects.nonNull(resource.getHttpMethod())) {
            matchMethod = resource.getHttpMethod().equals(method);
        }
        return matchUri && matchMethod;
    }

    private boolean matchAuthority(String resourceRole, List<GrantedAuthority> authorities) {
        String entityPrefix = getEntityPrefix(resourceRole, 0, resourceRole.indexOf("_"));
        Set<GrantedAuthority> grantedAuthorities = authorities.stream()
                .filter(grantedAuthority -> grantedAuthority.getAuthority()
                        .startsWith(entityPrefix)).collect(Collectors.toSet());
        if (!grantedAuthorities.isEmpty()) {
            String methodPrefix = getEntityPrefix(resourceRole, resourceRole.indexOf("_") + 1,
                    resourceRole.length());
            return grantedAuthorities.parallelStream().anyMatch(grantedAuthority -> getAuthorities(grantedAuthority).stream()
                    .anyMatch(charAuthority -> charAuthority.equals(methodPrefix)));
        }

        return false;
    }

    private String getEntityPrefix(String role, int begin, int end) {
        return role.substring(begin, end);
    }

    private List<String> getAuthorities(GrantedAuthority grantedAuthority) {
        String authority = grantedAuthority.getAuthority();
        return roleTokenizerWithSlash(authority.substring(authority.indexOf("_") + 1, authority.length()));
    }

    private List<String> roleTokenizerWithSlash(String role) {
        return BaseUtils.tokenizer(role, "_", false);
    }

    private List<String> charArrayToList(char[] array) {
        List<String> list = new ArrayList<>();
        for (char c : array) {
            list.add(String.valueOf(c));
        }
        return list;
    }

}
