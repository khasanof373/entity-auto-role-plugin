package uz.devops.khasanof.entityautoroleplugin.core.interceptor;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import uz.devops.khasanof.entityautoroleplugin.core.EntityCollector;
import uz.devops.khasanof.entityautoroleplugin.core.util.BaseUtils;
import uz.devops.khasanof.entityautoroleplugin.core.util.JwtUtils;
import uz.devops.khasanof.entityautoroleplugin.domain.Resource;
import uz.devops.khasanof.entityautoroleplugin.domain.Url;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Objects;

/**
 * @author Nurislom
 * @see uz.devops.khasanof.entityautoroleplugin.core.interceptor
 * @since 02.08.2023 15:04
 */
@Component
public class RequestHandler implements HandlerInterceptor {

    private final EntityCollector<Resource> resourceCollector;
    private final EntityCollector<Url> urlCollector;
    private final RequestChecker requestChecker;
    private final JwtUtils jwtUtils;

    public RequestHandler(EntityCollector<Resource> collector, EntityCollector<Url> urlCollector, RequestChecker requestChecker, JwtUtils jwtUtils) {
        this.resourceCollector = collector;
        this.urlCollector = urlCollector;
        this.requestChecker = requestChecker;
        this.jwtUtils = jwtUtils;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String requestURI = request.getRequestURI();
        RequestMethod requestMethod = RequestMethod.valueOf(request.getMethod());
        boolean isOpen = requestChecker.check(requestURI, requestMethod, urlCollector);

        if (isOpen) return true;

        String token = HttpRequestUtils.getToken(request);

        if (Objects.nonNull(token)) {
            List<GrantedAuthority> authorities = jwtUtils.getAuthorities(token);
            boolean check = requestChecker.check(requestURI, requestMethod, authorities, resourceCollector);
            HttpRequestUtils.predicateNoneMatch(() -> check, () -> HttpRequestUtils.responseWriter(response));
            return check;
        }

        HttpRequestUtils.predicateNoneMatch(() -> false, () -> HttpRequestUtils.responseWriter(response,
                HttpStatus.UNAUTHORIZED.value(), "Unauthorized"));
        return false;
    }

}
