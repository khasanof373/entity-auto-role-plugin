package uz.devops.khasanof.entityautoroleplugin.core.util;

import org.springframework.web.bind.annotation.*;
import uz.devops.khasanof.entityautoroleplugin.domain.EntityAuthorize;
import uz.devops.khasanof.entityautoroleplugin.domain.MethodAuthorize;
import uz.devops.khasanof.entityautoroleplugin.domain.UrlAuthorize;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * @author Nurislom
 * @see uz.devops.khasanof.entityautoroleplugin.core.util
 * @since 02.08.2023 11:27
 */
public abstract class BaseUtils {

    private static final Map<Map.Entry<String, Class<? extends Annotation>>, Supplier<String>> supplierMap = new HashMap<>();

    public static final Set<Class<? extends Annotation>> HTTP_ANNOTATIONS = Set.of(
            PostMapping.class, DeleteMapping.class, PutMapping.class, PatchMapping.class, GetMapping.class);

    private static final Map<Function<Annotation, Boolean>, Map.Entry<Function<Annotation, List<String>>,
            Function<Annotation, RequestMethod>>> FUNCTION_MAP = new HashMap<>() {{
        put((annotation -> annotation.annotationType().equals(RequestMapping.class)),
                Map.entry((annotation -> Arrays.asList(((RequestMapping) annotation).value())),
                        (annotation -> ((RequestMapping) annotation).method()[0])));
        put((annotation -> annotation.annotationType().equals(GetMapping.class)),
                Map.entry((annotation -> Arrays.asList(((GetMapping) annotation).value())),
                        (annotation -> RequestMethod.GET)));
        put((annotation -> annotation.annotationType().equals(DeleteMapping.class)),
                Map.entry((annotation -> Arrays.asList(((DeleteMapping) annotation).value())),
                        (annotation -> RequestMethod.DELETE)));
        put((annotation -> annotation.annotationType().equals(PostMapping.class)),
                Map.entry((annotation -> Arrays.asList(((PostMapping) annotation).value())),
                        (annotation -> RequestMethod.POST)));
        put((annotation -> annotation.annotationType().equals(PutMapping.class)),
                Map.entry((annotation -> Arrays.asList(((PutMapping) annotation).value())),
                        (annotation -> RequestMethod.PUT)));
        put((annotation -> annotation.annotationType().equals(PatchMapping.class)),
                Map.entry((annotation -> Arrays.asList(((PatchMapping) annotation).value())),
                        (annotation -> RequestMethod.PATCH)));
    }};

    static {
        supplierMap.put(Map.entry("create", PostMapping.class), () -> "C");
        supplierMap.put(Map.entry("update", PutMapping.class), () -> "U");
        supplierMap.put(Map.entry("partialUpdate", PatchMapping.class), () -> "U");
        supplierMap.put(Map.entry("getAll", GetMapping.class), () -> "R");
        supplierMap.put(Map.entry("count", GetMapping.class), () -> "R");
        supplierMap.put(Map.entry("get", GetMapping.class), () -> "R");
        supplierMap.put(Map.entry("delete", DeleteMapping.class), () -> "D");
    }

    public static <T> void getListMatchFunctionAccept(T var, Map<Function<T, Boolean>, Consumer<T>> functionMap) {
        functionMap.entrySet().stream().filter(functionFunctionEntry ->
                functionFunctionEntry.getKey().apply(var)).forEach(entry -> entry.getValue().accept(var));
    }

    public static <T, R> R getListMatchFunctionValue(T var, Map<Function<T, Boolean>, Function<T, R>> functionMap) {
        return functionMap.entrySet().stream().filter(functionFunctionEntry ->
                        functionFunctionEntry.getKey().apply(var)).map(entry -> entry.getValue().apply(var))
                .findFirst().orElse(null);
    }

    public static String concatDoublePrefix(String entityPrefix, String methodPrefix) {
        return entityPrefix.concat("_".concat(methodPrefix));
    }

    public static String getSupplierMap(Method method) {
        return supplierMap.entrySet().stream()
                .filter(entry -> method.getName().startsWith(entry.getKey().getKey()) &&
                        method.isAnnotationPresent(entry.getKey().getValue()))
                .map(entrySupplierEntry -> entrySupplierEntry.getValue().get()).findFirst().orElse(null);
    }

    public static boolean hasSupplierMap(Method method) {
        return supplierMap.entrySet().stream()
                .anyMatch(entry -> method.getName().startsWith(entry.getKey().getKey()) &&
                        method.isAnnotationPresent(entry.getKey().getValue()));
    }

    public static boolean hasSupplierMap(Method method, EntityAuthorize entityAuthorize) {
        return supplierMap.entrySet().parallelStream()
                .anyMatch(entry -> {
                    String methodName = entry.getKey().getKey() + entityAuthorize.getDomainClassType().getSimpleName();
                    return Objects.equals(methodName, method.getName());
                });
    }

    public static List<String> claimPathTokenizer(String path) {
        return Collections.list(new StringTokenizer(path, ".")).stream()
                .map(token -> (String) token)
                .collect(Collectors.toList());
    }

    public static List<String> roleTokenizer(String role) {
        return Collections.list(new StringTokenizer(role.substring(role.indexOf("_"),
                        role.length()), "_", false)).stream()
                .map(token -> (String) token)
                .collect(Collectors.toList());
    }

    public static List<String> tokenizer(String var, String delim, boolean returnDelim) {
        return Collections.list(new StringTokenizer(var, delim, returnDelim)).stream()
                .map(token -> (String) token)
                .collect(Collectors.toList());
    }

    public static String createRole(String entityPrefix, UrlAuthorize authorize) {
        return entityPrefix.concat("_" + authorize.getRoleSuffix());
    }

    public static String createRole(String entityPrefix, MethodAuthorize authorize) {
        return entityPrefix.concat("_" + authorize.getRoleSuffix());
    }

    public static void checkArgsIsNonNull(Object... args) {
        var anyMatch = Arrays.stream(args).anyMatch(Objects::isNull);
        if (anyMatch) {
            throw new NullPointerException("one or more param is null!");
        }
    }

    public static List<String> requestAnnotationValue(Annotation annotation) {
        for (var entry : FUNCTION_MAP.entrySet()) {
            if (entry.getKey().apply(annotation)) {
                return entry.getValue().getKey().apply(annotation);
            }
        }
        return new ArrayList<>();
    }

    public static RequestMethod requestAnnotationMethod(Annotation annotation) {
        for (var entry : FUNCTION_MAP.entrySet()) {
            if (entry.getKey().apply(annotation)) {
                return entry.getValue().getValue().apply(annotation);
            }
        }
        return null;
    }

}
