package uz.devops.khasanof.entityautoroleplugin.core.util;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nimbusds.jose.shaded.json.JSONArray;
import com.nimbusds.jose.shaded.json.JSONObject;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Nurislom
 * @see uz.devops.khasanof.entityautoroleplugin.core.util
 * @since 8/5/2023 1:40 PM
 */
@Component
public class JwtUtils {

    private final JwtDecoder jwtDecoder;
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Value("${entity.auto-role.claim-path:roles}")
    private String claimPath;

    public JwtUtils(JwtDecoder jwtDecoder) {
        this.jwtDecoder = jwtDecoder;
    }

    public List<GrantedAuthority> getAuthorities(String token) {
        Jwt decode = jwtDecoder.decode(token);
        List<String> paths = new ArrayList<>();
        if (!Objects.isNull(claimPath)) {
            paths.addAll(BaseUtils.claimPathTokenizer(claimPath));
        }
        return mapRolesToGrantedAuthorities(getRolesWithClaims(decode.getClaims(), paths));
    }

    @SuppressWarnings("unchecked")
    private List<String> getRolesWithClaims(Map<String, Object> objectMap, List<String> paths) {
        if (Objects.isNull(paths) || paths.isEmpty()) {
            return jsonArrayToList(((JSONArray) objectMap.getOrDefault("roles", new JSONArray())));
        } else {
            if (paths.size() == 1) {
                return (List<String>) objectMap.get(paths.get(0));
            } else {
                LinkedHashMap<String, JSONArray> toMap = jsonObjectToMap((JSONObject) objectMap.get(paths.get(0)));
                if (Objects.isNull(toMap)) {
                    return new ArrayList<>();
                }
                return jsonArrayToList(toMap.get(paths.get(1)));
            }
        }
    }

    private List<GrantedAuthority> mapRolesToGrantedAuthorities(Collection<String> roles) {
        return roles.stream().map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());
    }

    private List<String> jsonArrayToList(JSONArray jsonArray) {
        if (Objects.isNull(jsonArray)) {
            return new ArrayList<>();
        }
        return jsonArray.stream().map(obj -> (String) obj).collect(Collectors.toList());
    }

    @SneakyThrows
    private LinkedHashMap<String, JSONArray> jsonObjectToMap(JSONObject jsonObject) {
        return objectMapper.readValue(jsonObject.toJSONString(), new TypeReference<>() {
        });
    }
}
