package uz.devops.khasanof.entityautoroleplugin.core.writer;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import uz.devops.khasanof.entityautoroleplugin.domain.EntityAuthorize;
import uz.devops.khasanof.entityautoroleplugin.domain.Resource;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Objects;

import static uz.devops.khasanof.entityautoroleplugin.core.util.BaseUtils.HTTP_ANNOTATIONS;

/**
 * @author Nurislom
 * @see uz.devops.khasanof.entityautoroleplugin.core.writer
 * @since 8/6/2023 4:47 PM
 */
public abstract class AbstractWriter {

    protected boolean hasOtherConfig(EntityAuthorize entityAuthorize) {
        return (Objects.nonNull(entityAuthorize.getUrlAuthorizes()) && !entityAuthorize.getUrlAuthorizes().isEmpty())
                || (Objects.nonNull(entityAuthorize.getMethodAuthorizes()) && !entityAuthorize.getMethodAuthorizes().isEmpty());
    }

    protected boolean hasRequestMappingAnnotations(Class<?> clazz) {
        return clazz.isAnnotationPresent(RequestMapping.class);
    }

    public static boolean hasRequestMappingAnnotations(Annotation annotation) {
        if (annotation.annotationType().equals(RequestMapping.class)) {
            return true;
        } else {
            return HTTP_ANNOTATIONS.contains(annotation.annotationType());
        }
    }

    public static boolean hasRequestMappingAnnotations(Method method) {
        if (method.isAnnotationPresent(RequestMapping.class)) {
            return true;
        } else {
            if (method.getDeclaredAnnotations().length >= 1) {
                return Arrays.stream(method.getDeclaredAnnotations())
                        .anyMatch(annotation -> HTTP_ANNOTATIONS.contains(annotation.annotationType()));
            }
            return false;
        }
    }

    public static String concatDoubleSlash(String classUri, String methodUri) {
        return classUri.concat(methodUri)
                .replaceAll("//", "/");
    }

    protected Resource setUpResource(Resource resource, String urls) {
        resource.setUrl(urls);
        return resource;
    }

    protected Resource setUpResource(Resource resource, String urls, RequestMethod httpMethod) {
        setUpResource(resource, urls);
        resource.setHttpMethod(httpMethod);
        return resource;
    }

    protected Resource setUpResource(Resource resource, String urls, RequestMethod httpMethod, String role) {
        setUpResource(resource, urls, httpMethod);
        resource.setRole(role);
        return resource;
    }

}
