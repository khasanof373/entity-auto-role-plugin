package uz.devops.khasanof.entityautoroleplugin.core.writer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import uz.devops.khasanof.entityautoroleplugin.core.Writer;
import uz.devops.khasanof.entityautoroleplugin.domain.AbstractAuthorize;
import uz.devops.khasanof.entityautoroleplugin.domain.EntityAuthorize;
import uz.devops.khasanof.entityautoroleplugin.domain.MethodAuthorize;
import uz.devops.khasanof.entityautoroleplugin.domain.UrlAuthorize;
import uz.devops.libs.keycloak.admin.service.KeycloakAdminService;
import uz.devops.libs.keycloak.admin.service.dto.RoleRepresentationDTO;

import java.util.*;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * @author Nurislom
 * @see uz.devops.khasanof.entityautoroleplugin.core.writer
 * @since 01.08.2023 16:26
 */
@Slf4j
@Service
public class KeycloakWriter extends AbstractWriter implements Writer {

    private final KeycloakAdminService keycloakAdminService;

    public KeycloakWriter(KeycloakAdminService keycloakAdminService) {
        this.keycloakAdminService = keycloakAdminService;
    }

    @Override
    public void execute(Set<EntityAuthorize> authorizes) {
        log.info("##### keycloak writer start! #####");
        authorizes.stream().map(this::entityAuthorizeToRoles)
                .flatMap(Collection::stream)
                .forEach(role -> {
                    RoleRepresentationDTO roleRepresentationDTO = new RoleRepresentationDTO();
                    roleRepresentationDTO.setName(role);
                    try {
                        keycloakAdminService.createRole(roleRepresentationDTO);
                    } catch (RuntimeException e) {
                        log.warn("message : {}", e.getMessage());
                    }
                });
    }

    public List<String> entityAuthorizeToRoles(EntityAuthorize entityAuthorize) {
        if (entityAuthorize.isSetDefaultAuthorizes()) {
            if (Objects.isNull(entityAuthorize.getEntityPrefix())) {
                entityAuthorize.setEntityPrefix(entityAuthorize.getDomainClassType().getSimpleName().toUpperCase());
            }
            List<String> defaultRoles = new ArrayList<>(createDefaultRoles(entityAuthorize.getEntityPrefix()));
            if (hasOtherConfig(entityAuthorize)) {
                List<Map.Entry<Boolean, String>> otherAuthorizes = getOtherAuthorizes(entityAuthorize);
                otherAuthorizes.forEach(newAuthorize -> {
                    List<String> assembledList = new ArrayList<>();
                    reAddingExistRolesNewOne(defaultRoles,
                            newAuthorize, assembledList, entityAuthorize.getEntityPrefix());
                    defaultRoles.addAll(assembledList);
                });
            }
            return defaultRoles;
        } else {
            if (Objects.isNull(entityAuthorize.getEntityPrefix())) {
                entityAuthorize.setEntityPrefix(entityAuthorize.getDomainClassType().getSimpleName().toUpperCase());
            }
            return getOtherAuthorizesV2(entityAuthorize);
        }
    }

    private void reAddingExistRolesNewOne(List<String> defaultRoles, Map.Entry<Boolean, String> entry, List<String> assembledList, String entityPrefix) {
        assembledList.add(createRole(entityPrefix, entry.getValue()));
        if (entry.getKey()) {
            defaultRoles.forEach(defaultRole -> assembledList.add(createRole(defaultRole, entry.getValue())));
        }
    }

    private List<Map.Entry<Boolean, String>> getOtherAuthorizes(EntityAuthorize entityAuthorize) {
        if (Objects.nonNull(entityAuthorize.getMethodAuthorizes()) && !entityAuthorize.getMethodAuthorizes().isEmpty()) {
            return createAuthorizeRoles(entityAuthorize, entityAuthorize::getMethodAuthorizes,
                    MethodAuthorize::getRoleSuffix, false);
        } else {
            return createAuthorizeRoles(entityAuthorize, entityAuthorize::getUrlAuthorizes,
                    UrlAuthorize::getRoleSuffix, false);
        }
    }

    private List<String> getOtherAuthorizesV2(EntityAuthorize entityAuthorize) {
        if (Objects.nonNull(entityAuthorize.getMethodAuthorizes()) && !entityAuthorize.getMethodAuthorizes().isEmpty()) {
            return createAuthorizeRolesV2(entityAuthorize, entityAuthorize::getMethodAuthorizes, MethodAuthorize::getRoleSuffix);
        } else {
            return createAuthorizeRolesV2(entityAuthorize, entityAuthorize::getUrlAuthorizes, UrlAuthorize::getRoleSuffix);
        }
    }

    private List<String> createDefaultRoles(String entityPrefix) {
        return List.of(
                entityPrefix.concat("_R"), entityPrefix.concat("_R_C"), entityPrefix.concat("_R_U"),
                entityPrefix.concat("_R_D"), entityPrefix.concat("_R_C_U"), entityPrefix.concat("_R_C_D"),
                entityPrefix.concat("_R_U_C"), entityPrefix.concat("_R_U_D"), entityPrefix.concat("_R_D_C"),
                entityPrefix.concat("_R_D_U"), entityPrefix.concat("_R_C_U_D")
        );
    }

    private <T extends AbstractAuthorize> List<Map.Entry<Boolean, String>> createAuthorizeRoles(EntityAuthorize authorize, Supplier<List<T>> listSupplier,
                                                                            Function<T, String> methodSupplier, boolean addEntityPrefix) {

        return listSupplier.get().stream()
                .map(method -> {
                    if (addEntityPrefix) {
                        return Map.entry(method.isCombine(), createRole(authorize.getEntityPrefix(), methodSupplier.apply(method)));
                    } else {
                        return Map.entry(method.isCombine(), methodSupplier.apply(method));
                    }
                })
                .collect(Collectors.toCollection(ArrayList::new));
    }

    private <T extends AbstractAuthorize > List<String> createAuthorizeRolesV2(EntityAuthorize authorize, Supplier<List<T>> listSupplier,
                                                  Function<T, String> methodSupplier) {
        List<String> assembledList = new ArrayList<>();
        listSupplier.get()
                .forEach(method -> {
                    if (method.isCombine()) {
                        if (!assembledList.isEmpty()) {
                            List<String> reAdds = new ArrayList<>();
                            assembledList.forEach(assembled -> reAdds.add(createRole(assembled, methodSupplier.apply(method))));
                            assembledList.addAll(reAdds);
                        }
                    }
                    assembledList.add(createRole(authorize.getEntityPrefix(), methodSupplier.apply(method)));
                });
        return assembledList;
    }

    private String createRole(String entityPrefix, String rolePrefix) {
        return entityPrefix.concat("_" + rolePrefix);
    }

}
