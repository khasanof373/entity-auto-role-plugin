package uz.devops.khasanof.entityautoroleplugin.core.writer;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.framework.AopProxyUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import uz.devops.khasanof.entityautoroleplugin.core.EntityCollector;
import uz.devops.khasanof.entityautoroleplugin.core.Writer;
import uz.devops.khasanof.entityautoroleplugin.core.util.BaseUtils;
import uz.devops.khasanof.entityautoroleplugin.core.writer.resource.ResourceWriterAdapter;
import uz.devops.khasanof.entityautoroleplugin.domain.EntityAuthorize;
import uz.devops.khasanof.entityautoroleplugin.domain.Resource;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static uz.devops.khasanof.entityautoroleplugin.core.util.BaseUtils.getSupplierMap;

/**
 * @author Nurislom
 * @author Abdulloh
 * @see uz.devops.khasanof.entityautoroleplugin.core.writer
 * @since 01.08.2023 16:26
 */
@Slf4j
@Component
public class MainResourceWriter extends AbstractWriter implements Writer {

    private Set<Class<?>> classes;
    private final EntityCollector<Resource> collector;
    private final ResourceWriterAdapter resourceWriterAdapter;
    private final ApplicationContext applicationContext;

    public MainResourceWriter(EntityCollector<Resource> collector, ResourceWriterAdapter resourceWriterAdapter, ApplicationContext applicationContext) {
        this.collector = collector;
        this.resourceWriterAdapter = resourceWriterAdapter;
        this.applicationContext = applicationContext;
    }

    @Override
    @SneakyThrows
    public void execute(Set<EntityAuthorize> list) {
        propertiesSet();
        log.info("##### resource writer start! #####");
        if (list.isEmpty()) {
            log.warn("Resource list is empty or null!");
        } else {
            Set<Resource> allResource = getAllResource(list);
            log.info("written resource size : {}", allResource.size());
            collector.addEntity(allResource);
        }
    }

    private void propertiesSet() {
        this.classes = this.applicationContext.getBeansWithAnnotation(RestController.class).values().stream()
                .map(AopProxyUtils::ultimateTargetClass).collect(Collectors.toSet());
    }

    public Set<Resource> getAllResource(Set<EntityAuthorize> authorizes) {
        return authorizes.stream().map(this::getResourcesWithEntity)
                .filter(Objects::nonNull).flatMap(Collection::stream)
                .collect(Collectors.toSet());
    }

    private Collection<Resource> getResourcesWithEntity(EntityAuthorize authorize) {
        String targetClassName = authorize.getDomainClassType().getSimpleName();
        if (Objects.isNull(authorize.getEntityPrefix())) {
            authorize.setEntityPrefix(targetClassName.toUpperCase());
        }
        if (Objects.isNull(classes) || classes.isEmpty()) {
            log.warn("RestController class null or empty!");
            return new HashSet<>();
        }
        return classes.stream().filter(controller -> controller.getSimpleName()
                        .toLowerCase(Locale.ROOT).startsWith(targetClassName.toLowerCase()))
                .map(controller -> createResources(controller.getDeclaredMethods(),
                        controller, authorize))
                .findFirst().orElse(new HashSet<>());
    }

    private Collection<Resource> createResources(Method[] methods, Class<?> clazz, EntityAuthorize authorize) {

        if (Objects.isNull(methods) || methods.length == 0) {
            return new HashSet<>();
        }

        List<String> classLevelUris = new ArrayList<>();
        if (hasRequestMappingAnnotations(clazz)) {
            classLevelUris.addAll(Arrays.asList(clazz.getAnnotation(RequestMapping.class).value()));
        }

        Set<Resource> resources = new HashSet<>();
        if (authorize.isSetDefaultAuthorizes()) {
            resources.addAll(
                    Arrays.stream(methods)
                            .filter(AbstractWriter::hasRequestMappingAnnotations)
                            .filter(method -> BaseUtils.hasSupplierMap(method, authorize))
                            .map(method -> getAnnotationsValue(method.getDeclaredAnnotations(),
                                    classLevelUris, getMethodPrefix(method, authorize.getEntityPrefix())))
                            .flatMap(Collection::stream).collect(Collectors.toSet())
            );
        }

        if (hasOtherConfig(authorize)) {
            resources.addAll(resourceWriterAdapter.writer(methods, classLevelUris, authorize));
        }

        return resources;
    }

    private String getMethodPrefix(Method method, String entityPrefix) {
        String supplierMap = getSupplierMap(method);
        if (Objects.nonNull(supplierMap)) {
            return BaseUtils.concatDoublePrefix(entityPrefix, supplierMap);
        }
        return null;
    }

    private List<Resource> getAnnotationsValue(Annotation[] annotations, List<String> classLevelUris, String methodPrefix) {
        Map<Function<Annotation, Boolean>, Function<Annotation, List<Resource>>> functionMap = new HashMap<>() {
            {
                put((annotation -> annotation instanceof RequestMapping), (annotation -> {
                    RequestMapping mapping = (RequestMapping) annotation;
                    return combineClassAndMethodUris(getNullOrFirst(mapping),
                            Arrays.asList(mapping.value()), classLevelUris, methodPrefix);
                }));
                put((annotation -> annotation instanceof PostMapping), (annotation -> {
                    PostMapping mapping = (PostMapping) annotation;
                    return combineClassAndMethodUris(RequestMethod.POST,
                            Arrays.asList(mapping.value()), classLevelUris, methodPrefix);
                }));
                put((annotation -> annotation instanceof DeleteMapping), (annotation -> {
                    DeleteMapping mapping = (DeleteMapping) annotation;
                    return combineClassAndMethodUris(RequestMethod.DELETE,
                            Arrays.asList(mapping.value()), classLevelUris, methodPrefix);
                }));
                put((annotation -> annotation instanceof PutMapping), (annotation -> {
                    PutMapping mapping = (PutMapping) annotation;
                    return combineClassAndMethodUris(RequestMethod.PUT,
                            Arrays.asList(mapping.value()), classLevelUris, methodPrefix);
                }));
                put((annotation -> annotation instanceof GetMapping), (annotation -> {
                    GetMapping mapping = (GetMapping) annotation;
                    return combineClassAndMethodUris(RequestMethod.GET,
                            Arrays.asList(mapping.value()), classLevelUris, methodPrefix);
                }));
                put((annotation -> annotation instanceof PatchMapping), (annotation -> {
                    PatchMapping mapping = (PatchMapping) annotation;
                    return combineClassAndMethodUris(RequestMethod.PATCH,
                            Arrays.asList(mapping.value()), classLevelUris, methodPrefix);
                }));
            }

            private RequestMethod getNullOrFirst(RequestMapping mapping) {
                return mapping.method().length >= 1 ? mapping.method()[0] : null;
            }

        };
        return Arrays.stream(annotations)
                .filter(AbstractWriter::hasRequestMappingAnnotations)
                .map(annotation -> BaseUtils.getListMatchFunctionValue(annotation, functionMap))
                .filter(Objects::nonNull).flatMap(Collection::stream).collect(Collectors.toList());
    }

    private List<Resource> combineClassAndMethodUris(RequestMethod method, List<String> methodUris, List<String> classUris, String prefix) {
        if (!classUris.isEmpty()) {
            return classUris.stream().flatMap(classUri -> methodUris.stream()
                            .map(methodUri -> concatDoubleSlash(classUri, methodUri)))
                    .map(uri -> setUpResource(new Resource(), uri, method, prefix))
                    .collect(Collectors.toList());
        } else {
            return methodUris.stream().map(methodUri -> setUpResource(new Resource(), methodUri, method))
                    .collect(Collectors.toList());
        }
    }

}
