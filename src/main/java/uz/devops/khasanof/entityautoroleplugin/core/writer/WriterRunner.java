package uz.devops.khasanof.entityautoroleplugin.core.writer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import uz.devops.khasanof.entityautoroleplugin.core.Writer;
import uz.devops.khasanof.entityautoroleplugin.core.collector.EntityAuthorizeCollector;

import java.util.Map;

/**
 * @author Nurislomcore
 * @see uz.devops.khasanof.entityautorolepluigin
 * @since 01.08.2023 16:23
 */
@Slf4j
@Service
public class WriterRunner implements ApplicationRunner {

    @Autowired
    private ApplicationContext applicationContext;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("##### writer runner start ####");
        EntityAuthorizeCollector entityCollector = applicationContext.getBean(EntityAuthorizeCollector.class);
        Map<String, Writer> beans = applicationContext.getBeansOfType(Writer.class);
        beans.values().forEach(beanWriter -> beanWriter.execute(entityCollector.getAll()));
    }
}
