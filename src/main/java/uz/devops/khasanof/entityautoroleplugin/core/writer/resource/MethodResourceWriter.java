package uz.devops.khasanof.entityautoroleplugin.core.writer.resource;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;
import uz.devops.khasanof.entityautoroleplugin.core.writer.AbstractWriter;
import uz.devops.khasanof.entityautoroleplugin.domain.EntityAuthorize;
import uz.devops.khasanof.entityautoroleplugin.domain.MethodAuthorize;
import uz.devops.khasanof.entityautoroleplugin.domain.Resource;

import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

import static uz.devops.khasanof.entityautoroleplugin.core.util.BaseUtils.*;
import static uz.devops.khasanof.entityautoroleplugin.core.writer.AbstractWriter.concatDoubleSlash;
import static uz.devops.khasanof.entityautoroleplugin.core.writer.AbstractWriter.hasRequestMappingAnnotations;

/**
 * @author Nurislom
 * @see uz.devops.khasanof.entityautoroleplugin.core.writer.resource
 * @since 8/8/2023 11:56 AM
 */
@Component(value = MethodResourceWriter.NAME)
public class MethodResourceWriter implements ResourceWriter {

    public static final String NAME = "methodResourceWriter";

    @Override
    public Set<Resource> resourceWriter(Method[] methods, List<String> classLevelUris, EntityAuthorize entityAuthorize) {
        return matchMethodCollector(methods, classLevelUris, entityAuthorize);
    }

    private Set<Resource> matchMethodCollector(Method[] methods, List<String> classLevelUris, EntityAuthorize entityAuthorize) {
        return entityAuthorize.getMethodAuthorizes().stream().map(methodAuthorize -> {
            Method fMethod = Arrays.stream(methods)
                    .filter(method -> matchMethodAndArgs(methodAuthorize, method))
                    .findFirst().orElse(null);
            if (Objects.nonNull(fMethod)) {
                return methodGetAllResources(fMethod, classLevelUris,
                        concatDoublePrefix(entityAuthorize.getEntityPrefix(), methodAuthorize.getRoleSuffix()));
            }
            return new HashSet<Resource>();
        }).flatMap(Collection::stream).collect(Collectors.toSet());
    }

    private Set<Resource> methodGetAllResources(Method method, List<String> classLevelUris, String fullPrefix) {
        boolean mappingAnnotations = hasRequestMappingAnnotations(method);
        if (mappingAnnotations) {
            return Arrays.stream(method.getDeclaredAnnotations())
                    .filter(AbstractWriter::hasRequestMappingAnnotations)
                    .map(annotation -> {
                        List<String> annotationValue = requestAnnotationValue(annotation);
                        if (annotationValue.isEmpty()) {
                            return new HashSet<Resource>();
                        }
                        RequestMethod requestMethod = requestAnnotationMethod(annotation);
                        return classLevelUris.stream().map(uri -> annotationValue.stream()
                                        .map(value -> new Resource(fullPrefix, requestMethod,
                                                concatDoubleSlash(uri, value))).collect(Collectors.toSet()))
                                .flatMap(Collection::stream).collect(Collectors.toCollection(HashSet::new));
                    }).flatMap(Collection::stream).collect(Collectors.toSet());
        }
        return new HashSet<>();
    }

    private boolean matchMethodAndArgs(MethodAuthorize methodAuthorize, Method method) {
        boolean name, params;
        name = method.getName().equals(methodAuthorize.getMethodName());
        if (Objects.isNull(methodAuthorize.getArgsTypes())) {
            params = true;
        } else {
            List<Class<?>> list = Arrays.asList(method.getParameterTypes());
            params = Arrays.stream(methodAuthorize.getArgsTypes()).allMatch(list::contains);
        }
        return name && params;
    }

}
