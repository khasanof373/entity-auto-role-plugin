package uz.devops.khasanof.entityautoroleplugin.core.writer.resource;

import uz.devops.khasanof.entityautoroleplugin.domain.EntityAuthorize;
import uz.devops.khasanof.entityautoroleplugin.domain.Resource;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Set;

/**
 * @author Nurislom
 * @see uz.devops.khasanof.entityautoroleplugin.core.writer.resource
 * @since 8/8/2023 12:32 PM
 */
public interface ResourceWriter {

    Set<Resource> resourceWriter(Method[] methods, List<String> classLevelUris, EntityAuthorize entityAuthorize);

}
