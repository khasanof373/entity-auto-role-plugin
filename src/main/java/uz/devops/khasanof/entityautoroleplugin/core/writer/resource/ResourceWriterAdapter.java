package uz.devops.khasanof.entityautoroleplugin.core.writer.resource;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;
import uz.devops.khasanof.entityautoroleplugin.domain.EntityAuthorize;
import uz.devops.khasanof.entityautoroleplugin.domain.Resource;

import java.lang.reflect.Method;
import java.util.*;

/**
 * @author Nurislom
 * @see uz.devops.khasanof.entityautoroleplugin.core.writer.resource
 * @since 8/8/2023 12:26 PM
 */
@Service
public class ResourceWriterAdapter implements ApplicationContextAware {

    private final Map<String, ResourceWriter> writerMap = new HashMap<>();

    public Set<Resource> writer(Method[] methods, List<String> classLevelUris, EntityAuthorize entityAuthorize) {
        return writerMap.get(getBeanName(entityAuthorize)).resourceWriter(methods, classLevelUris, entityAuthorize);
    }

    private String getBeanName(EntityAuthorize authorize) {
        if (Objects.isNull(authorize.getMethodAuthorizes())) {
            return UrlResourceWriter.NAME;
        } else {
            return MethodResourceWriter.NAME;
        }
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        writerMap.putAll(applicationContext.getBeansOfType(ResourceWriter.class));
    }
}
