package uz.devops.khasanof.entityautoroleplugin.core.writer.resource;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;
import uz.devops.khasanof.entityautoroleplugin.core.util.BaseUtils;
import uz.devops.khasanof.entityautoroleplugin.core.writer.AbstractWriter;
import uz.devops.khasanof.entityautoroleplugin.domain.EntityAuthorize;
import uz.devops.khasanof.entityautoroleplugin.domain.Resource;
import uz.devops.khasanof.entityautoroleplugin.domain.UrlAuthorize;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

import static uz.devops.khasanof.entityautoroleplugin.core.util.BaseUtils.concatDoublePrefix;
import static uz.devops.khasanof.entityautoroleplugin.core.writer.AbstractWriter.concatDoubleSlash;

/**
 * @author Nurislom
 * @see uz.devops.khasanof.entityautoroleplugin.core.writer.resource
 * @since 8/8/2023 11:54 AM
 */
@Component(value = UrlResourceWriter.NAME)
public class UrlResourceWriter implements ResourceWriter {

    public static final String NAME = "urlResourceWriter";

    @Override
    public Set<Resource> resourceWriter(Method[] methods, List<String> classLevelUris, EntityAuthorize entityAuthorize) {
        Map<UrlAuthorize, Method> methodCollector = matchMethodCollector(methods, classLevelUris,
                entityAuthorize.getUrlAuthorizes());
        return methodCollector.keySet().stream().map(method -> new Resource(concatDoublePrefix(entityAuthorize.getEntityPrefix(),
                        method.getRoleSuffix()), method.getHttpMethod(), method.getUrl()))
                .collect(Collectors.toSet());
    }

    private Map<UrlAuthorize, Method> matchMethodCollector(Method[] methods, List<String> classLevelUris,
                                                           List<UrlAuthorize> urlAuthorizes) {
        return urlAuthorizes.stream().map(urlAuthorize -> {
            Method fMethod = Arrays.stream(methods)
                    .filter(method -> matchAnnotationURI(urlAuthorize, method.getDeclaredAnnotations(),
                            classLevelUris)).findFirst().orElse(null);
            if (Objects.nonNull(fMethod)) {
                return new AbstractMap.SimpleEntry<>(urlAuthorize, fMethod);
            }
            return null;
        }).filter(Objects::nonNull).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    private boolean matchAnnotationURI(UrlAuthorize urlAuthorize, Annotation[] annotations, List<String> classUris) {
        return classUris.stream().anyMatch(classUri -> Arrays.stream(annotations)
                .anyMatch(annotation -> {
                    boolean match = AbstractWriter.hasRequestMappingAnnotations(annotation);
                    if (match) {
                        if (Objects.nonNull(urlAuthorize.getHttpMethod())) {
                            return BaseUtils.requestAnnotationValue(annotation)
                                    .stream().anyMatch(annotationUri -> {
                                        boolean methodMatch = false;
                                        RequestMethod requestMethod = BaseUtils.requestAnnotationMethod(annotation);
                                        if (Objects.nonNull(requestMethod)) {
                                            methodMatch = requestMethod.equals(urlAuthorize.getHttpMethod());
                                        }
                                        return concatDoubleSlash(classUri, annotationUri)
                                                .equals(urlAuthorize.getUrl()) && methodMatch;
                                    });
                        }
                        return BaseUtils.requestAnnotationValue(annotation)
                                .stream().anyMatch(annotationUri -> concatDoubleSlash(classUri, annotationUri)
                                        .equals(urlAuthorize.getUrl()));
                    }
                    return false;
                }));
    }

}
