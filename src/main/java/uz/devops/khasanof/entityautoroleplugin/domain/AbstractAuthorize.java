package uz.devops.khasanof.entityautoroleplugin.domain;

import lombok.*;

/**
 * @author Nurislom
 * @see uz.devops.khasanof.entityautoroleplugin.domain
 * @since 9/12/2023 8:26 PM
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
public abstract class AbstractAuthorize {

    private boolean isCombine;

}
