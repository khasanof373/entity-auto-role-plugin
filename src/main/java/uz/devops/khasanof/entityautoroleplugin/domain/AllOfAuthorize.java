package uz.devops.khasanof.entityautoroleplugin.domain;

import lombok.NoArgsConstructor;

/**
 * @author Nurislom
 * @see uz.devops.khasanof.entityautoroleplugin.domain
 * @since 11/3/2023 11:49 PM
 */
@NoArgsConstructor
public class AllOfAuthorize extends UndefinedAuthorize {

    public AllOfAuthorize(Class<?>[] entities, String role) {
        super(entities, role);
    }

    public AllOfAuthorize entities(Class<?>... entities) {
        super.setEntities(entities);
        return this;
    }

    public AllOfAuthorize role(String role) {
        super.setRole(role);
        return this;
    }

}
