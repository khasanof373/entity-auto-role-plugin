package uz.devops.khasanof.entityautoroleplugin.domain;

import lombok.NoArgsConstructor;

/**
 * @author Nurislom
 * @see uz.devops.khasanof.entityautoroleplugin.domain
 * @since 11/3/2023 11:54 PM
 */
@NoArgsConstructor
public class CrudOfAuthorize extends UndefinedAuthorize {

    public CrudOfAuthorize(Class<?>[] entities, String role) {
        super(entities, role);
    }

    public CrudOfAuthorize entities(Class<?>... entities) {
        super.setEntities(entities);
        return this;
    }

    public CrudOfAuthorize role(String role) {
        super.setRole(role);
        return this;
    }

}
