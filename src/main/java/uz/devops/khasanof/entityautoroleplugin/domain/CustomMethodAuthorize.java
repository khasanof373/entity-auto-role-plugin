package uz.devops.khasanof.entityautoroleplugin.domain;

import lombok.*;

import java.util.Objects;

/**
 * @author Nurislom
 * @see uz.devops.khasanof.entityautoroleplugin.domain
 * @since 11/7/2023 11:11 PM
 */
@NoArgsConstructor
public class CustomMethodAuthorize {

    private Method method;
    private String role;

    public CustomMethodAuthorize(String name) {
        this.method = new Method().name(name);
    }

    public CustomMethodAuthorize(String name, Class<?>[] args, String role) {
        this.method = new Method(name, args);
        this.role = role;
    }

    public CustomMethodAuthorize name(String name) {
        checkNonNull();
        this.method.setName(name);
        return this;
    }

    public CustomMethodAuthorize args(Class<?>... args) {
        checkNonNull();
        this.method.args(args);
        return this;
    }

    public CustomMethodAuthorize role(String role) {
        this.role = role;
        return this;
    }

    void checkNonNull() {
        if (Objects.isNull(this.method)) {
            this.method = new Method();
        }
    }

}
