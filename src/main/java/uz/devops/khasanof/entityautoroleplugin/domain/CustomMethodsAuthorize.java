package uz.devops.khasanof.entityautoroleplugin.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author Nurislom
 * @see uz.devops.khasanof.entityautoroleplugin.domain
 * @since 11/7/2023 11:47 PM
 */
@Data
@NoArgsConstructor
public class CustomMethodsAuthorize {

    private List<Method> methods;
    private String role;

    public CustomMethodsAuthorize(String[] methodNames, String role) {
        this.methods = createMethods(methodNames);
        this.role = role;
    }

    public CustomMethodsAuthorize methodNames(String... methodNames) {
        this.methods = createMethods(methodNames);
        return this;
    }

    public CustomMethodsAuthorize methods(Method... methods) {
        this.methods = Arrays.asList(methods);
        return this;
    }

    public CustomMethodsAuthorize role(String role) {
        this.role = role;
        return this;
    }

    private List<Method> createMethods(String... names) {
        return Arrays.stream(names).map(name -> new Method().name(name))
                .collect(Collectors.toCollection(ArrayList::new));
    }

    void checkNonNull() {
        if (Objects.isNull(methods)) {
            this.methods = new ArrayList<>();
        }
    }

}
