package uz.devops.khasanof.entityautoroleplugin.domain;

import lombok.*;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Objects;

/**
 * @author Nurislom
 * @see uz.devops.khasanof.entityautoroleplugin.domain
 * @since 11/3/2023 11:55 PM
 */
@NoArgsConstructor
public class CustomUriAuthorize {

    private Url uri;
    private String role;

    public CustomUriAuthorize(String uri, RequestMethod httpMethod, String role) {
        this.uri = new Url(uri, httpMethod);
        this.role = role;
    }

    public CustomUriAuthorize uri(String uri) {
        checkNonNull();
        this.uri.setUri(uri);
        return this;
    }

    public CustomUriAuthorize httpMethod(RequestMethod httpMethod) {
        checkNonNull();
        this.uri.setHttpMethod(httpMethod);
        return this;
    }

    public CustomUriAuthorize role(String role) {
        this.role = role;
        return this;
    }

    void checkNonNull() {
        if (Objects.isNull(this.uri)) {
            this.uri = new Url();
        }
    }

}
