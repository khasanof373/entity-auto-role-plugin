package uz.devops.khasanof.entityautoroleplugin.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Nurislom
 * @see uz.devops.khasanof.entityautoroleplugin.domain
 * @since 11/8/2023 12:09 AM
 */
@Data
@NoArgsConstructor
public class CustomUrisAuthorize {

    private List<Url> urls;
    private String role;

    public CustomUrisAuthorize(Url[] urls, String role) {
        this.urls = Arrays.asList(urls);
        this.role = role;
    }

    public CustomUrisAuthorize uris(String... uris) {
        this.urls = createUrl(uris);
        return this;
    }

    public CustomUrisAuthorize urls(Url... urls) {
        this.urls = Arrays.asList(urls);
        return this;
    }

    public CustomUrisAuthorize role(String role) {
        this.role = role;
        return this;
    }

    private List<Url> createUrl(String... uris) {
        return Arrays.stream(uris).map(Url::new)
                .collect(Collectors.toCollection(ArrayList::new));
    }

}
