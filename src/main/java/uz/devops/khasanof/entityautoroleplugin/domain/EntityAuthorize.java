package uz.devops.khasanof.entityautoroleplugin.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * @author Nurislom
 * @see uz.devops.khasanof.entityautoroleplugin.domain
 * @since 31.07.2023 16:56
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
public class EntityAuthorize {

    private Class<?> domainClassType;
    private boolean setDefaultAuthorizes;
    private String entityPrefix;
    private List<MethodAuthorize> methodAuthorizes;
    private List<UrlAuthorize> urlAuthorizes;

    public EntityAuthorize(Class<?> domainClassType, boolean setDefaultAuthorizes) {
        this.domainClassType = domainClassType;
        this.setDefaultAuthorizes = setDefaultAuthorizes;
    }

    public EntityAuthorize(Class<?> domainClassType, boolean setDefaultAuthorizes, String entityPrefix) {
        this.domainClassType = domainClassType;
        this.setDefaultAuthorizes = setDefaultAuthorizes;
        this.entityPrefix = entityPrefix;
    }

    public EntityAuthorize customize(UrlAuthorize... urlAuthorizes) {
        this.urlAuthorizes = List.of(urlAuthorizes);
        return this;
    }

    public EntityAuthorize customize(MethodAuthorize... methodAuthorizes) {
        this.methodAuthorizes = List.of(methodAuthorizes);
        return this;
    }

}
