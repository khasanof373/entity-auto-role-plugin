package uz.devops.khasanof.entityautoroleplugin.domain;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @author Nurislom
 * @see uz.devops.khasanof.entityautoroleplugin.domain
 * @since 11/7/2023 11:51 PM
 */
@Data
@NoArgsConstructor
public class Method {

    private String name;
    private Class<?>[] args;

    public Method(String name, Class<?>[] args) {
        this.name = name;
        this.args = args;
    }

    public Method name(String name) {
        this.name = name;
        return this;
    }

    public Method args(Class<?>... args) {
        this.args = args;
        return this;
    }

}
