package uz.devops.khasanof.entityautoroleplugin.domain;

import lombok.*;

/**
 * @author Nurislom
 * @see uz.devops.khasanof.entityautoroleplugin.domain
 * @since 31.07.2023 16:59
 */
@Getter
@Setter
@ToString
public class MethodAuthorize extends AbstractAuthorize {

    private String methodName;
    private Class<?>[] argsTypes;
    private String roleSuffix;

    public MethodAuthorize(String methodName, String roleSuffix) {
        this(methodName, roleSuffix, false);
    }

    public MethodAuthorize(String methodName, String roleSuffix, boolean isCombine) {
        this(methodName, null, roleSuffix, isCombine);
    }

    public MethodAuthorize(String methodName, Class<?>[] argsTypes, String roleSuffix) {
        this(methodName, argsTypes, roleSuffix, false);
    }

    public MethodAuthorize(String methodName, Class<?>[] argsTypes, String roleSuffix, boolean isCombine) {
        super(isCombine);
        this.methodName = methodName;
        this.argsTypes = argsTypes;
        this.roleSuffix = roleSuffix;
    }

}
