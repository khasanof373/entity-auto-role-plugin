package uz.devops.khasanof.entityautoroleplugin.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Objects;

@Data
@ToString
@AllArgsConstructor
public class Resource {

    private String role;
    private RequestMethod httpMethod;
    private String url;

    public Resource() {
    }

    public Resource(String role, String url) {
        this.role = role;
        this.url = url;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Resource resource = (Resource) o;

        if (httpMethod != resource.httpMethod) return false;
        return Objects.equals(url, resource.url);
    }

    @Override
    public int hashCode() {
        int result = httpMethod != null ? httpMethod.hashCode() : 0;
        result = 31 * result + (url != null ? url.hashCode() : 0);
        return result;
    }
}
