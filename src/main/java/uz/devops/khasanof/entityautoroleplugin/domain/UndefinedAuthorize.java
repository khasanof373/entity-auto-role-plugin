package uz.devops.khasanof.entityautoroleplugin.domain;

import lombok.*;

/**
 * @author Nurislom
 * @see uz.devops.khasanof.entityautoroleplugin.domain
 * @since 11/3/2023 11:52 PM
 */
@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public abstract class UndefinedAuthorize {

    private Class<?>[] entities;
    private String role;

}
