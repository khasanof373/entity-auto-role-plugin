package uz.devops.khasanof.entityautoroleplugin.domain;

import lombok.*;

/**
 * @author Nurislom
 * @see uz.devops.khasanof.entityautoroleplugin.domain
 * @since 11/4/2023 12:04 AM
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public abstract class UndefinedRole {

    private String role;

}
