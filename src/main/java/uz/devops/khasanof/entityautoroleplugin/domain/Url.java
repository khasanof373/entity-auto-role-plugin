package uz.devops.khasanof.entityautoroleplugin.domain;

import lombok.*;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Objects;

/**
 * @author Nurislom
 * @see uz.devops.khasanof.entityautoroleplugin.domain
 * @since 8/17/2023 11:53 AM
 */
@Data
@NoArgsConstructor
public class Url {

    private String uri;
    private RequestMethod httpMethod;

    public Url(String uri) {
        this.uri = uri;
    }

    public Url(String uri, RequestMethod httpMethod) {
        this.uri = uri;
        this.httpMethod = httpMethod;
    }

    public Url uri(String url) {
        this.uri = url;
        return this;
    }

    public Url httpMethod(RequestMethod httpMethod) {
        this.httpMethod = httpMethod;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Url url1 = (Url) o;

        if (!Objects.equals(uri, url1.uri)) return false;
        return httpMethod == url1.httpMethod;
    }

    @Override
    public int hashCode() {
        int result = uri != null ? uri.hashCode() : 0;
        result = 31 * result + (httpMethod != null ? httpMethod.hashCode() : 0);
        return result;
    }
}
