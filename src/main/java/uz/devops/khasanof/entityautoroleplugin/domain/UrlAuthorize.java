package uz.devops.khasanof.entityautoroleplugin.domain;

import lombok.*;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author Nurislom
 * @see uz.devops.khasanof.entityautoroleplugin.domain
 * @since 31.07.2023 17:12
 */
@Getter
@Setter
@ToString
public class UrlAuthorize extends AbstractAuthorize {

    private String url;
    private RequestMethod httpMethod;
    private String roleSuffix;

    public UrlAuthorize(String url, String roleSuffix) {
        this(url, roleSuffix, false);
    }

    public UrlAuthorize(String url, String roleSuffix, boolean isCombine) {
       this(url, null, roleSuffix, isCombine);
    }

    public UrlAuthorize(String url, RequestMethod httpMethod, String roleSuffix) {
        this(url, httpMethod, roleSuffix, false);
    }

    public UrlAuthorize(String url, RequestMethod httpMethod, String roleSuffix, boolean isCombine) {
        super(isCombine);
        this.url = url;
        this.httpMethod = httpMethod;
        this.roleSuffix = roleSuffix;
    }

}
